# Transformation Functions

This repository can be used within the strategies folder of the [OSMP Framework](https://gitlab.com/tuda-fzd/perception-sensor-modeling/modular-osmp-framework).
It contains several functions for coordinate transformations of OSI3 objects.
